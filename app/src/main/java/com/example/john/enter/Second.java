package com.example.john.enter;

        import android.content.Intent;
        import android.support.v4.content.ContextCompat;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.TextView;

public class Second extends AppCompatActivity implements View.OnClickListener {
    private String login;
    Intent nIntent;
    TextView info;
    TextView typeLog;
    EditText secondLogin;
    Button buttonSecond;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        nIntent=getIntent();
        info= (TextView) findViewById(R.id.hellous);
        login =nIntent.getStringExtra(MainActivity.FLAG);
        info.setText(getString(R.string.second_Hello)+" "+login);

        typeLog=(TextView) findViewById(R.id.typelog);
        secondLogin=(EditText) findViewById(R.id.secondLogin);
        buttonSecond = (Button) findViewById(R.id.buttonSecond);
        buttonSecond.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        String getLogin = String.valueOf(secondLogin.getText());
        if (!"".equals(getLogin)) {
            Intent intent = new Intent();
            intent.putExtra("name1", getLogin);
            setResult(RESULT_OK, intent);
            finish();
        }
        else {
            typeLog.setTextColor(ContextCompat.getColor(Second.this, R.color.colorAccent));
        }
    }
}
